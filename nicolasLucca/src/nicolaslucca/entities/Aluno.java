package nicolaslucca.entities;

import java.util.HashSet;

public class Aluno extends Pessoa {

    private static int horasMax = 28;
    private int horas;
    private HashSet<Disciplina> disciplinas = new HashSet<>();
    
    public Aluno(String n, int id) {
        super(n, id);
    }

    public int getHorasMax() {
        return horasMax;
    }

    public void addHoras(int h) {
        int total = horas + h;
        if (total > horasMax) {
            System.out.println("Deu Bret Cupinxa");
        } else {
            horas += h;
        }
    }
    
    public void addDisciplinas(Disciplina d){
        if(d.getCargaHoraria()+horas <= horasMax){
            disciplinas.add(d);
            addHoras(d.getCargaHoraria());
        }
    }
    
    public StringBuilder getDisciplinas(){
        StringBuilder a = new StringBuilder();
        disciplinas.forEach(p ->{
            a.append(p.getDescricao()+" ");
        } );
        return a;
    }
    
    public int getHoras(){
        return horas;
    }

    public void removeDisciplina(Disciplina d){
        disciplinas.remove(d);
        horas-=d.getCargaHoraria();
    }
    
    public HashSet<Disciplina> getDis(){
        return disciplinas;
    }
    
    @Override
    public String toString() {
        return "[Nome]: " + getNome() + "     [Matricula]: " + getId() + "     [Carga Horária]: " + getHoras() + "     [Disciplinas]: " + getDisciplinas();
    }
}