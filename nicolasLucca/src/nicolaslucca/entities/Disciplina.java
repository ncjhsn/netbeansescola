package nicolaslucca.entities;

import java.util.HashSet;

public class Disciplina {
    private int id;
    private String descricao;
    private int cargaHoraria;
    private int modulo; //alunos max
    private HashSet<Professor> professores = new HashSet<>();
    private HashSet<Aluno> alunos = new HashSet<>();

    public Disciplina(int id, String descricao, int cargaHoraria, int modulo) {
        this.id = id;
        this.descricao = descricao;
        this.cargaHoraria = cargaHoraria;
        this.modulo = modulo;
    }

    public void addAlunos(Aluno a) {
        if (alunos.size() + 1 <= modulo) {
            alunos.add(a);
        }
    }

    public int getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    public int getModulo() {
        return modulo;
    }

    public void addProf(Professor p) {
        professores.add(p);
    }
    
    public void removeProf(Professor p){
        professores.remove(p);
    }

    public HashSet<Aluno> getAlunos(){
        return alunos;
    }

    public HashSet<Professor> getProfs(){
        return professores;
    }
    
    public void removeAluno(Aluno a){
        alunos.remove(a);
    }
    
    @Override
    public String toString(){
        return descricao;
    }
    
}