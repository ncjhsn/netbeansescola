package nicolaslucca.entities;

import java.util.HashSet;

public class Turma {
    private Disciplina disciplina;
    private int id;
    private Professor p;
    private HashSet<Aluno> alunos;

    public Turma(Disciplina disciplina, int id) {
        this.disciplina = disciplina;
        this.id = id;
        alunos = disciplina.getAlunos();
        
    }
    
    public Disciplina getDisciplina(){
        return disciplina;
    }

    public int getId(){
        return id;
    }

    public void setProfessor(Professor p){
        this.p = p;
    }

    public String getAlunos() {
        return alunos.toString();
    }

    public Professor getProfessor() {
        return p;
    }

    public void removeProfessor(Professor p){
        p = null;
    }

    public String toString(){
        return "Id: "+id+ "   Disciplina: "+disciplina + "   Quantidade de Alunos:" +disciplina.getAlunos().size();
    }
}