package nicolaslucca.entities;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class Professor extends Pessoa {

    private Scanner in = new Scanner(System.in);
    private int cargaHoraria;
    private int horasMax = 12;
    private HashSet<Disciplina> disciplinas = new HashSet<>();
    private HashMap<Disciplina, Turma> aulasLecionadas = new HashMap<>();
    public Professor(String n, int id) {
        super(n, id);
        cargaHoraria = 0;
    }

    public void addTurma(Disciplina d, Turma t) {
        if (cargaHoraria + d.getCargaHoraria() <= horasMax) {
            if (!aulasLecionadas.containsKey(d)) {
                aulasLecionadas.put(d, t);
                cargaHoraria += d.getCargaHoraria();
            } else {
                System.out.println("");
            }
        }
    }
    
    
    
    public int getId(){
        return id;
    }

    public void removeTurma(Disciplina d){
        if(aulasLecionadas.containsKey(d)){
            cargaHoraria -= d.getCargaHoraria();
            aulasLecionadas.remove(d);
        }
    }

    public void getTurmas() {
        if(aulasLecionadas.size() > 0) {
            aulasLecionadas.forEach((d, t) -> System.out.println("Disciplina: " + d.getDescricao() + ", Turma: " + t.getId()));
        }else{
            System.out.println("Este professor nao tem nenhuma turma");
        }
    }

    public StringBuilder getTurma(){
        StringBuilder s = new StringBuilder();
        if(aulasLecionadas.size()>0){
            aulasLecionadas.forEach((d,t)-> s.append(t.getId()));
        }
        return s;
    }
    
    public void addDisciplina(Disciplina d){
        disciplinas.add(d);
    }
    
    public void removeDisciplina(Disciplina d){
        disciplinas.remove(d);
    }
    
    public HashSet<Disciplina> getDisciplinas(){
        return disciplinas;
    }
    
    public int getHorasMax() {
        return horasMax;
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    @Override
    public String toString() {
        return "[Nome]: " + getNome() + "      Turmas: " +getTurma() + "        Id:" +getId() + "      Disciplinas: "+getDisciplinas().toString();
    }

}