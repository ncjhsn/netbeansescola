package nicolaslucca.entities;

public class Pessoa {
    public String nome;
    public int id;

    public Pessoa(String n, int id){
        nome = n;
        this.id = id;
    }

    public String getNome(){
        return nome;
    }

    public int getId(){
        return id;
    }


}
